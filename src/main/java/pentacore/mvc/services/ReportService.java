package pentacore.mvc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import pentacore.mvc.model.Report;
import pentacore.mvc.model.Survey;
import pentacore.mvc.repository.SurveyRepository;
import java.util.concurrent.Future;

/**
 * Created by Aras on 2017-05-13.
 */
@Service("ReportService")
public class ReportService {

    @Autowired
    private SurveyRepository surveyRepository;


    @Async
    public Future<Survey> LongOperation(Long id){

            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(30000);
                Survey survey = surveyRepository.findById(id);
                System.out.println(survey.getSurveyName());
                return new AsyncResult<Survey>(survey);

            } catch (InterruptedException e) {
                throw new RuntimeException();
            }
    }

    public ResponseEntity<Report> CreateReport(long id){

        System.out.println("Fetching Survey with id " + id);
        Survey survey = surveyRepository.findById(id);
        if (survey == null) {
            System.out.println("Survey with id " + id + " not found");
            return new ResponseEntity<Report>(HttpStatus.NOT_FOUND);
        }

        Report report = new Report(survey);
        return new ResponseEntity<Report>(report, HttpStatus.OK);
    }

}
