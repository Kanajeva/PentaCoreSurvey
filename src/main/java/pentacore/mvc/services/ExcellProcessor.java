package pentacore.mvc.services;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pentacore.mvc.model.Answer;
import pentacore.mvc.model.Question;
import pentacore.mvc.model.Survey;
import pentacore.mvc.repository.SurveyRepository;

import java.io.*;
import java.util.*;

/**
 * Created by Aras on 2017-05-27.
 */
@Service
public class ExcellProcessor {

    @Autowired
    private SurveyRepository surveyRepository;

    private  List<List<String>> importDataHeader = new ArrayList<>();
    private  List<List<String>> importDataSurvey = new ArrayList<>();
    private  List<List<String>> importDataAnswer = new ArrayList<>();


    private DataFormatter formatter = new DataFormatter();
    private  List<String> valueList;

    public  void ReadFromExcel(InputStream file) {

        Survey survey = new Survey();

        try {

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            ReadData(sheet,importDataHeader);
            HandleSurveyHeader(survey,importDataHeader);

            sheet = workbook.getSheetAt(1);
            ReadData(sheet,importDataSurvey);
            HandleDataSurvey(survey, importDataSurvey);

            System.out.println(survey.getOpen());
            file.close();

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void ReadData(XSSFSheet sheet, List<List<String>> importData ){

        try{
            importData.clear();
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            //Rows
            int rowNumber = sheet.getPhysicalNumberOfRows();

            for (int i = 0; i < rowNumber; i++){

                List<String> tempList = new ArrayList<>();
                valueList = new ArrayList<>();

                int index = 0;
                for (int j = 1; j < sheet.getRow(i).getPhysicalNumberOfCells(); j++){

                    String j_test = formatter.formatCellValue(sheet.getRow(i).getCell(j));

                    if (j_test != "") {
                        tempList.add(index, j_test);
                    }
                    index++;
                }
                valueList.add(0, formatter.formatCellValue(sheet.getRow(i).getCell(0)));

                int indx = 1;
                for (String str : tempList){
                    valueList.add(indx, str);
                    indx++;
                }
                importData.add(valueList);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void HandleDataSurvey(Survey survey ,List<List<String>> importData){

        for (int i = 1; i < importData.size(); i++){
            System.out.println("ciklo skaicius "+ importData.size());
            System.out.println("ciklo skaicius2 "+ importData.get(i).size());
            Question question = new Question();

            if(importData.get(i).get(3).equals("TEXT")){

                question.setType("area");
                question.setQuestion(importData.get(i).get(2));
                question.setRequired(true);

                if(importData.get(i).get(1).equals("YES")){
                    question.setRequired(true);
                }
                else{
                    question.setRequired(false);
                }

                Answer answer1 = new Answer();
                answer1.setAnswerString("area text1");
                answer1.setQuestionId(question);
                question.getAnswerList().add(answer1);

                Answer answer2 = new Answer();
                answer2.setAnswerString("area text2");
                answer2.setQuestionId(question);
                question.getAnswerList().add(answer2);
            }
            if(importData.get(i).get(3).equals("CHECKBOX")){
                question.setType("one");
                question.setQuestion(importData.get(i).get(2));
                question.setRequired(true);
                if(importData.get(i).get(1).equals("YES")){
                    question.setRequired(true);
                }
                else{
                    question.setRequired(false);
                }

                for (int j = 4; j < importData.get(i).size(); j++){

                    Answer answer = new Answer();
                    answer.setAnswerString(importData.get(i).get(j));
                    answer.setQuestionId(question);
                    question.getAnswerList().add(answer);
                }

            }
            if(importData.get(i).get(3).equals("MULTIPLECHOICE")){
                question.setType("many");
                question.setQuestion(importData.get(i).get(2));
                question.setRequired(true);

                if(importData.get(i).get(1).equals("YES")){
                    question.setRequired(true);
                }
                else{
                    question.setRequired(false);
                }

                for (int j = 4; j < importData.get(i).size(); j++){

                    Answer answer = new Answer();
                    answer.setAnswerString(importData.get(i).get(j));
                    answer.setQuestionId(question);
                    question.getAnswerList().add(answer);
                }

            }
            if(importData.get(i).get(3).equals("SCALE")){
                question.setType("integer");
                question.setQuestion(importData.get(i).get(2));
                question.setRequired(true);

                if(importData.get(i).get(1).equals("YES")){
                    question.setRequired(true);
                }
                else{
                    question.setRequired(false);
                }

                Answer answer1 = new Answer();
                answer1.setAnswerString(importData.get(i).get(4));
                answer1.setQuestionId(question);
                question.getAnswerList().add(answer1);

                Answer answer2 = new Answer();
                answer2.setAnswerString(importData.get(i).get(5));
                answer2.setQuestionId(question);
                question.getAnswerList().add(answer2);

            }
            System.out.println(question.getQuestion());
            question.setSurveyId(survey);
            survey.getQuestionList().add(question);
        }
        surveyRepository.save(survey);
    }

    private void HandleSurveyHeader(Survey survey ,List<List<String>> importData) {

        survey.setSurveyName(importData.get(0).get(1));
        survey.setSurveyDescription(importData.get(1).get(1));

        if(importData.get(3).get(1).equals("YES")){
            survey.setOpen(true);
        }
        else{
            survey.setOpen(false);
        }

    }

}
