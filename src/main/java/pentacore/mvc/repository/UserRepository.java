package pentacore.mvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pentacore.mvc.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findById(Long id );

    @Query("select u from User u where u.email like %?1")
    User findByEmail(String email);
}
