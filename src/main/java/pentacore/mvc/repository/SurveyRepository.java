package pentacore.mvc.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pentacore.mvc.model.Survey;
import pentacore.mvc.model.User;

import java.util.List;

/**
 * Created by Aras on 2017-04-08.
 */
public interface SurveyRepository extends JpaRepository<Survey, Long> {
    Survey findById(long id);
    List<Survey> findAllByUserId(User user);
}
