package pentacore.mvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pentacore.mvc.model.Answer;

/**
 * Created by Aras on 2017-05-20.
 */
public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
