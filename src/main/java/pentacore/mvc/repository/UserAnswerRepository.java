package pentacore.mvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pentacore.mvc.model.Useranswer;

/**
 * Created by Aras on 2017-05-27.
 */
public interface UserAnswerRepository extends JpaRepository<Useranswer, Long> {
}
