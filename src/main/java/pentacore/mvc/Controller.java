package pentacore.mvc;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pentacore.mvc.model.*;
import pentacore.mvc.repository.*;
import pentacore.mvc.services.ApplicationMailer;
import pentacore.mvc.services.ExcellProcessor;
import pentacore.mvc.services.ReportService;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Aras on 2017-04-23.
 */
@RestController
public class Controller {

    // Laukai
    private Future<Survey> future;

    //Repositorijos

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private UserRepository userRepository;

    //Servisai

    @Autowired
    @Qualifier("mailServiceCool")
    private ApplicationMailer mailer;

    @Autowired
    private ExcellProcessor excellProcessor;

    @Autowired
    private ReportService reportService;

    // Metodai

    @RequestMapping("/test")
    public void test(){

        System.out.println("Suveike testas");
    }

    @Transactional
    @RequestMapping(value = "/surveys/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Survey>> listAllSurveys() {

        List<Survey> surveys = surveyRepository.findAll();
        if(surveys.isEmpty()){
            return new ResponseEntity<List<Survey>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }

        for(Survey survey : surveys) {
            int max = 0;
            for (Question q : survey.getQuestionList()) {
                for (Answer a : q.getAnswerList()) {
                    Hibernate.initialize(a.getUserAnswerList());
                    if (max < a.getUserAnswerList().size()) {
                        max = a.getUserAnswerList().size();
                    }
                }
            }

            survey.countAnswer = max;
            survey.countQuestion = survey.getQuestionList().size();
        }
        return new ResponseEntity<List<Survey>>(surveys, HttpStatus.OK);
    }

    @RequestMapping(value = "/allusers/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listAllUsers() {

        List<User> users = userRepository.findAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "/surveys/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Survey> getSurvey(@PathVariable("id") long id) {

        System.out.println("Fetching Survey with id " + id);
        Survey survey = surveyRepository.findById(id);
        if (survey == null) {
            System.out.println("Survey with id " + id + " not found");
            return new ResponseEntity<Survey>(HttpStatus.NOT_FOUND);
        }

        int max = 0;
        for (Question q : survey.getQuestionList()){
            for (Answer a : q.getAnswerList()){
                Hibernate.initialize(a.getUserAnswerList());
                if(max < a.getUserAnswerList().size()){
                    max = a.getUserAnswerList().size();
                }
                }
            }

        survey.countAnswer = max;
        survey.countQuestion = survey.getQuestionList().size();
        return new ResponseEntity<Survey>(survey, HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "/newsurvey/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Survey> postSurvey(@RequestBody Survey survey) {

        for (Question q : survey.getQuestionList()){
            q.setSurveyId(survey);
            for (Answer a : q.getAnswerList()){
                a.setQuestionId(q);
            }
        }
        survey.setUserId(userRepository.findByEmail(survey.userEmail));
        surveyRepository.save(survey);

        return new ResponseEntity<Survey>(survey, HttpStatus.OK);
    }

    @RequestMapping(value = "/newuser/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> postUser(@RequestBody User user) {

        userRepository.save(user);
        mailer.sendMail(user.getEmail(), "Password link","http://localhost:8080/web-app/survey/#!/signup/"+ user.getId().toString());

        return new ResponseEntity<User>(user,HttpStatus.OK);
    }

    //http://localhost:8080/web-app/survey/#!/signup/5
    @RequestMapping(value = "/updateuser/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@RequestBody User user) {

        User currentUser = userRepository.findById(user.getId());
        //User currentUser = userRepository.findById(user.getId());
        currentUser.setName(user.getName());
        currentUser.setSurname(user.getSurname());
        currentUser.setPassword(user.getPassword());
        currentUser.setAdmin(false);
        currentUser.setBlocked(false);

        userRepository.save(currentUser);
        return new ResponseEntity<User>(currentUser,HttpStatus.OK);
    }

    @RequestMapping(value = "/login/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@RequestBody User user) {

        System.out.println(user.getEmail());
        User currentuser = userRepository.findByEmail(user.getEmail());
        System.out.println(currentuser.getEmail());
        currentuser.setSurveyList(surveyRepository.findAllByUserId(currentuser));
        System.out.println(currentuser.getPassword().equals(user.getPassword()));
        System.out.println(currentuser.getBlocked());

        if(currentuser == null || !(currentuser.getPassword().equals(user.getPassword())) || currentuser.getBlocked()){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<User>(currentuser,HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/long/", method = RequestMethod.GET)
    public ResponseEntity<Survey> longOp() throws ExecutionException, InterruptedException {

//        System.out.println(Thread.currentThread().getName());
//        Callable<String> callable = report::LongOperation;
//        return callable;
//        DeferredResult<ResponseEntity<Report>> deferredResult = new DeferredResult<>();
//        CompletableFuture.supplyAsync(report::LongOperation)
//                .whenCompleteAsync((result, throwable) -> deferredResult.setResult(new ResponseEntity<Report>(result,HttpStatus.ACCEPTED)));
//        System.out.println("Paleido");
//        return deferredResult;

//        if (reportService.getReport().getFinished().equals("")) {
//            return new ResponseEntity<Report>(HttpStatus.ACCEPTED);
//        } else return new ResponseEntity<Report>(reportService.getReport(), HttpStatus.OK);

        System.out.println( Thread.currentThread().getName() );
        if(future == null) {

            future = reportService.LongOperation(28L);
            return new ResponseEntity<Survey>(HttpStatus.OK);
        }
        if (future.isDone()) {
            System.out.println("Result from asynchronous process - " );
            return new ResponseEntity<Survey>(future.get(),HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Survey>(HttpStatus.ACCEPTED);
        }
    }

    @RequestMapping(value = "/updateadmin/", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateAdmin(@RequestBody User user) {

        try{
            userRepository.save(user);
        }catch (ObjectOptimisticLockingFailureException exception){
            System.out.println("Pagavau Optimistic klaida");
            return new ResponseEntity<User>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<User>(user,HttpStatus.OK);
    }

    @RequestMapping(value = "/answer/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Survey> postAnswer(@RequestBody Survey survey) {

        for (Question q : survey.getQuestionList()){
            q.setSurveyId(survey);
            for (Answer a : q.getAnswerList()){
                a.setQuestionId(q);
                for(Useranswer ua : a.getUserAnswerList()){
                        ua.setAnswerId(a);
                }
            }
        }

        survey.setUserId(userRepository.findByEmail(survey.userEmail));
        surveyRepository.save(survey);
        return new ResponseEntity<Survey>(survey, HttpStatus.OK);
    }

//    @RequestMapping(value = "/excel/", method = RequestMethod.POST)
//    public void excel(@RequestPart("excelFile") MultipartFile file){
//
//
//    }

    @Transactional
    @RequestMapping(value = "/report/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Report> getReport(@PathVariable("id") long id) {

        return reportService.CreateReport(id);
    }

    @RequestMapping(value = "/remember/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> rememberPassword(@RequestBody User user) {

        System.out.println(user.getEmail());
        User currentuser = userRepository.findByEmail(user.getEmail());
        System.out.println(currentuser.getEmail());
        if(currentuser == null){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        mailer.sendMail(user.getEmail(), "Password","Your password: "+ currentuser.getPassword().toString());
        return new ResponseEntity<User>(user,HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Survey> deleteSurvey(@PathVariable("id") long id) {

        surveyRepository.delete(id);
        return new ResponseEntity<Survey>( HttpStatus.OK);
    }

    @RequestMapping(value = "/uploadExcelFile/" , method = RequestMethod.POST)
    public HttpStatus uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        System.out.println(file.getOriginalFilename());
        InputStream in = file.getInputStream();
        excellProcessor.ReadFromExcel(in);
        return HttpStatus.OK;
    }

}
