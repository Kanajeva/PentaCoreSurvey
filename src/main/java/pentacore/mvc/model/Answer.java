package pentacore.mvc.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aras on 2017-04-08.
 */
@Entity
@Table(name = "answer")
public class Answer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonProperty
    public Long id;

    @Column(name = "answerstring")
    private String answerString;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    private Question questionId;

    @JsonIgnore
    @OneToMany(mappedBy = "answerId",cascade = CascadeType.ALL)
    private List<Useranswer> userAnswerList = new ArrayList<Useranswer>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswerString() {
        return answerString;
    }

    public void setAnswerString(String answerString) {
        this.answerString = answerString;
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    @JsonIgnore
    public List<Useranswer> getUserAnswerList() {
        return userAnswerList;
    }

    @JsonProperty
    public void setUserAnswerList(List<Useranswer> userAnswerList) {
        this.userAnswerList = userAnswerList;
    }
}
