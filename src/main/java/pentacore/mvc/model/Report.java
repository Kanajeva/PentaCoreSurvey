package pentacore.mvc.model;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import pentacore.mvc.repository.SurveyRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aras on 2017-05-13.
 */
public class Report implements Serializable {

    private String surveyName;

    private String surveyDescription;

    private List<QuestionData> questionNameList = new ArrayList<QuestionData>();

    public Report(Survey survey) {

        surveyName = survey.getSurveyName();
        surveyDescription = survey.getSurveyDescription();

        for (Question q : survey.getQuestionList()) {
            QuestionData questionData = new QuestionData(q.getQuestion(), q.getType());
            questionNameList.add(questionData);
            if (q.getType().equals("integer")) {
                int sum = 0;
                for(Answer a : q.getAnswerList()) {
                    AnswerData answerData = new AnswerData(a.getAnswerString());
                    answerData.answerCount = 0;
                    questionData.answerDataList.add(answerData);
                }
                Answer a = q.getAnswerList().get(0);
                for (Useranswer ua : a.getUserAnswerList()) {
                    System.out.println(ua.getResponse());
                    sum += Integer.parseInt(ua.getResponse());
                }
                int size = a.getUserAnswerList().size();
                if(size==0){
                    AnswerData answerData = new AnswerData(Integer.toString(0));
                    answerData.answerCount = 0;
                    questionData.answerDataList.add(answerData);
                }
                else{
                    AnswerData answerData = new AnswerData(Integer.toString(sum / size));
                    answerData.answerCount = size;
                    questionData.answerDataList.add(answerData);
                }

            } else if (q.getType().equals("area")) {
                Answer a = q.getAnswerList().get(0);
                for(Useranswer ua : a.getUserAnswerList()){
                    AnswerData answerData = new AnswerData(ua.getResponse());
                    answerData.answerCount = 1;
                    questionData.answerDataList.add(answerData);
                }

            } else {
                for (Answer a : q.getAnswerList()) {
                    AnswerData answerData = new AnswerData(a.getAnswerString());
                    Hibernate.initialize(a.getUserAnswerList());
                    answerData.answerCount = a.getUserAnswerList().size();
                    questionData.answerDataList.add(answerData);
                }
            }
        }
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String name) {
        this.surveyName = name;
    }

    public String getSurveyDescription() {
        return surveyDescription;
    }

    public void setSurveyDescription(String surveyDescription) {
        this.surveyDescription = surveyDescription;
    }

    public List<QuestionData> getQuestionNameList() {
        return questionNameList;
    }

    public void setQuestionNameList(List<QuestionData> questionNameList) {
        this.questionNameList = questionNameList;
    }
}

class QuestionData{

    public String question;
    public String questionType;
    public List<AnswerData> answerDataList = new ArrayList<AnswerData>();


    public QuestionData(String question, String questionType) {
        this.question = question;
        this.questionType = questionType;
    }
}

class AnswerData {

    public String answer;
    public int answerCount = 0;

    public AnswerData(String answer) {
        this.answer = answer;
    }
}
