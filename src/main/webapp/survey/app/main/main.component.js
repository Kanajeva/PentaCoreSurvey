(function () {
    var app = angular.module("app.main");

    app.component("main", {
        templateUrl: "app/main/main.template.html",
    });
})();
 