(function () {
    var app = angular.module("app.reminder");

    app.component("reminder", {
        templateUrl: "app/reminder/reminder.template.html",
        controller: function (reminderService, $state) {
            var vm = this;

            vm.done = false;

            vm.remind = function () {
                vm.done = true;
                reminderService.remind(vm.email).then(
                    function(data) {
                        vm.done = true;
                    },
                    function(reason) {
                        console.log(reason);
                    }
                );
            };
        }
    });

})();