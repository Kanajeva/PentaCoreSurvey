(function () {
    var module = angular.module("app.reminder");

    module.service("reminderService", function ($http, $q) {
        this.remind = function (token) {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/remember/",
                data: {email: token}
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };

    });

})();