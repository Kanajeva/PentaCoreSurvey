(function () {
    var app = angular.module("app.report");

    app.component("report", {
        templateUrl: "app/report/report.template.html",
        controller: function ($localStorage, reportService, $state) {
            var vm = this;
            vm.$storage = $localStorage.$default();
            vm.types = [{
                val: 'one',
                name: 'Vienas galimas pasirinkimas'
            },
                {
                    val: 'many',
                    name: 'Keli galimi pasirinkimai'
                },
                {
                    val: 'area',
                    name: 'Laisvas tekstas'
                },
                {
                    val: 'integer',
                    name: 'Sveikas skaičius'
                }];
            vm.survey = {};

            vm.$onInit = function () {
                if (!vm.$storage.view) {
                    $state.go('login');
                }

                reportService.load($state.params.id).then(function(response) {
                    vm.survey = response;
                }, function(reason) {
                    console.log(reason);
                });
            };
        }
    });
})();
