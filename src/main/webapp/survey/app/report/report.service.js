(function () {
    var module = angular.module("app.report");

    module.service("reportService", function ($http, $q) {

        this.load = function (token) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: "http://localhost:8080/web-app/report/" + token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };
    })

})();
