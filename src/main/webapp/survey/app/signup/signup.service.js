(function () {
    var module = angular.module("app.signup");

    module.service("signupService", function ($http, $q) {
        this.signup = function (token) {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/newuser/",
                data: {email: token}
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };     

    });

})();
