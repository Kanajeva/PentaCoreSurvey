(function () {
    var app = angular.module('app.header');

    app.component("header", {
        templateUrl: "app/header/header.template.html",
        controller: function ($state, $localStorage) {
            var vm = this;
            vm.$storage = $localStorage.$default();

            vm.logout = function() {
                vm.$storage.view = false;
                $state.go("login");
            }
        }
    });
})();