(function () {
    var module = angular.module("app.surveyAnswer");

    module.service("surveyAnswerService", function ($http, $q) {

        this.load = function (token) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: "http://localhost:8080/web-app/surveys/" + token,
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };

        this.done = function (token) {
            var deferred = $q.defer();
            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/answer/",
                data: token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };

    })

})();
