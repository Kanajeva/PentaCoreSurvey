(function () {
    var app = angular.module("app");


    app.config(function ($stateProvider, $urlRouterProvider) {
        var loginState = {
            name: 'login',
            url: '/login',
            component: 'login'
        };

        var signupState = {
            name: 'signup',
            url: '/signup',
            component: 'signup'
        };

        var signupFinalState = {
            name: 'signupFinal',
            url: '/signup/:id',
            component: 'signupFinal',
            params: {
                id: null
            }
        };

        var reminderState = {
            name: 'reminder',
            url: '/reminder',
            component: 'reminder'
        };

        var contentState = {
            abstract: true,
            name: 'content',
            url: '/app',
            component: 'content'
        };

        var manageState = {
            name: 'manage',
            parent: 'content',
            url: '/manage',
            component: 'manage'
        };

        var surveyState = {
            name: 'survey',
            parent: 'content',
            url: '/survey',
            component: 'survey'
        };

        var surveyAnswerState = {
            name: 'surveyAnswer',
            parent: 'content',
            url: '/surveyAnswer/:id',
            component: 'surveyAnswer',
            params: {
                id: null
            }
        };

        var reportState = {
            name: 'report',
            parent: 'content',
            url: '/report/:id',
            component: 'report',
            params: {
                id: null
            }
        };

        var viewState = {
            name: 'viewSurveys',
            parent: 'content',
            url: '/view',
            component: 'viewSurveys'
        };
          
        $stateProvider.state(loginState);
        $stateProvider.state(signupState);
        $stateProvider.state(signupFinalState);
        $stateProvider.state(reminderState);
        $stateProvider.state(contentState);
        $stateProvider.state(manageState);
        $stateProvider.state(surveyState);
        $stateProvider.state(surveyAnswerState);
        $stateProvider.state(reportState);
        $stateProvider.state(viewState);
        
        $urlRouterProvider.otherwise('/login');
    });
})();