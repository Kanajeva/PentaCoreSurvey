(function () {
    var app = angular.module("app.viewSurveys");

    app.component("viewSurveys", {
        templateUrl: "app/viewSurveys/viewSurveys.template.html",
        controller: function ($state, $localStorage, viewSurveysService) {
            var vm = this;
            vm.$storage = $localStorage.$default();
            vm.surveyList = {};

            vm.types = [{
                    val: 'surveyName',
                    name: 'Pavadinimą'
                },
                {
                    val: 'userEmail',
                    name: 'Autorių'
                },
                {
                    val: 'createDate',
                    name: 'Datą'
                }];

            vm.sortOption = [{
                    val: false,
                    name: 'Didėjančiai'
                },
                {
                    val: true,
                    name: 'Mažėjančiai'
                }];

            vm.sortBy = "surveyName";
            vm.sortOpt = false;
            vm.searchBy = "surveyName";
            vm.searchText = "";

            vm.$onInit = function () {
                if (!vm.$storage.view) {
                    $state.go('login');
                }
                vm.load();
            };

            vm.load = function(){
                viewSurveysService.loadSurevys().then(function(response) {
                    vm.surveyList = response;
                }, function(reason) {
                    console.log(reason);
                });
            };

            vm.viewSurvey = function(index) {
                $state.go('surveyAnswer', {
                    id: index
                });
            };

            vm.viewReport = function(index) {
                $state.go('report', {
                    id: index
                });
            };

            vm.deleteSurvey = function(index){
                viewSurveysService.deleteSurvey(index).then(function() {
                    viewSurveysService.loadSurevys().then(function(response) {
                        vm.surveyList = response;
                    }, function(reason) {
                        console.log(reason);
                    });
                }, function(reason) {
                    console.log(reason);
                });
            };

             vm.search = function(row){
                 return !!((row.surveyName.indexOf(vm.searchText || '') !== -1 || row.createDate.indexOf(vm.searchText || '') !== -1 || row.userEmail.indexOf(vm.searchText || '') !== -1));
            };

             vm.uploadFiles = function(file){
                 console.log(file);
                 viewSurveysService.uploadFiles(file).then(function(response) {
                     viewSurveysService.loadSurevys().then(function(response) {
                         vm.surveyList = response;
                     }, function(reason) {
                         console.log(reason);
                     });
                 }, function(reason) {
                     console.log(reason);
                 });
            };
        }

    });
})();
 