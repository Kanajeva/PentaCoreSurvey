(function () {
var app = angular.module("app.content");
    
    app.component("content", {
        templateUrl: "app/content/content.template.html",
        controller: function ($localStorage) {
            var vm = this;
            vm.$storage = $localStorage.$default();
        }
    });
})();
 