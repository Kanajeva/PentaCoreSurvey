(function () {
    var app = angular.module("app.login");

    app.component("login", {
        templateUrl: "app/login/login.template.html",
        controller: function (loginService, $state, localStorageService, $localStorage) {
            var vm = this;
            vm.$storage = $localStorage.$default();

            vm.logIn = function () {
                loginService.login(vm.data).then(function(response) {
                    vm.$storage.admin = response.admin;
                    vm.$storage.userName = response.name;
                    vm.$storage.userSurname = response.surname;
                    vm.$storage.email = response.email;
                    vm.$storage.view = true;
                    $state.go('viewSurveys');
                }, function(reason) {
                    console.log(reason);
                });
            }
        }
    })

})();
