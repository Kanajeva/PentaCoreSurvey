(function () {
    var module = angular.module("app.login");

    module.service("loginService", function ($http, $q) {

        this.login = function (token) {
            var deferred = $q.defer();
            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/login/",
                data: token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };     

    })

})();
