(function () {
var app = angular.module("app.manage");

    app.component("manage", {
        templateUrl: "app/manage/manage.template.html",
        controller: function (signupService, $state, $localStorage, manageService) {
            vm = this;
            vm.$storage = $localStorage.$default();
            vm.userList = [];
            vm.showMessage = false;

            vm.$onInit = function () {
                if (!vm.$storage.view) {
                    $state.go('login');
                }
                vm.load();

            };

            vm.load =function(){
                manageService.loadUsers().then(function(response) {
                    vm.userList = response;
                }, function(reason) {
                    console.log(reason);
                });
            };

            vm.createUser = function(formValidation){
                if(formValidation) {
                    signupService.signup(vm.newUser).then(
                        function (data) {
                            vm.newUser = "";
                        },
                        function (reason) {
                            console.log(reason);
                        }
                    );
                }
            };

            vm.update = function (index) {
                manageService.updateUsers(vm.userList[index]).then(function() {
                    vm.load();
                }, function(reason) {
                    if (reason.status == 409) {
                        vm.showMessage = true;
                    }
                });
            };
        }
    });
})();
 