(function () {
    var app = angular.module("app.sidenav");

    app.component("sidenav", {
        templateUrl: "app/sidenav/sidenav.template.html",
        controller: function ($state, $localStorage) {
            var vm = this;
            vm.$storage = $localStorage.$default();
        }
    });
})();
 