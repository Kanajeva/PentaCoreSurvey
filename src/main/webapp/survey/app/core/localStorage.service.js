(function () {
    var module = angular.module("app");
    module.factory("localStorageService", function () {

        var service = {
            addItem: addItem,
            removeItem: removeItem,
            getItem: getItem
        };

        return service;

        function addItem(key, data) {
            if (typeof (data) === "object") {
                localStorage.setItem(key, JSON.stringify(data));
            } else {
                localStorage.setItem(key, data);
            }
        }

        function removeItem(key) {
            localStorage.removeItem(key);
        }

        function getItem(key) {
            return localStorage.getItem(key);
        }

    })
})();