var express = require('express');
var app = express();
var path = require('path');

app.use('/app', express.static('app'));
app.use('/node_modules', express.static('node_modules'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(7123, function () {
    console.log('Server has started!');
});
